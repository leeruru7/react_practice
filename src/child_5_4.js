import React, {Component} from 'react';

class Child extends Component {

    render() {
        // 解構
        const {count, addParentCount, addChildCount} = this.props;
        return (
            <div>
                <h1>Child: {count}</h1>
                <button onClick={addParentCount}>+parent</button>
                <button onClick={addChildCount}>+child</button>
            </div>
        );
    }
}

export default Child;
