import logo from './logo.svg';
import './App.css';
import {useState} from 'react';

function App() {
    const [count, setCount] = useState(1);
    const func = () => {
        console.log('fuc')
    }
    return (
        <div className="App">
            <header className="App-header">
                <img src={logo} className="App-logo" alt="logo"/>
                <p>
                    It's Jane's first react app! {count}
                </p>
                <button onClick={func}>click me</button>
                <a
                    className="App-link"
                    href="https://reactjs.org"
                    target="_blank"
                    rel="noopener noreferrer"
                >
                    Learn React
                </a>
            </header>
        </div>
    );
}

// must close
// class -> className
// for -> htmlFor
export default App;
