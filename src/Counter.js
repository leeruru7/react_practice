import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Counter extends Component {
    // 設定 prop 初始值 方法1
    // static defaultProps = {
    //     initCount: 0
    // }
    // 設定 prop 值的類型 方法1
    // static propTypes = {
    //     initCount: PropTypes.number
    // }


    // 用 props 指定初始 state
    constructor(props) {
        super(props); // 就是 counter 這個 component
        this.state = {
            count: props.initCount,
        }
    }

    addCount = () => {
        // 這裡花括號是傳入物件
        // this.setState({
        //     count: this.state.count + 1
        // })

        // 原本上方傳入物件的寫法，連續寫三次一樣只會count+1，因為setState是非同步的關係(三個setState同時進行，不會累加1)
        // 改寫傳入函式，然後連續重複三次這段，就可以count+3
        this.setState(state => ({
            // 舊的 state + 1
            count: state.count + 1,
        }))
        this.setState(state => ({
            // 舊的 state + 1
            count: state.count + 1,
        }))
        this.setState(state => ({
            // 舊的 state + 1
            count: state.count + 1,
        }))
    }

    render() {
        return (
            <div>
                <h1>{this.state.count}</h1>
                <button onClick={this.addCount}>+3</button>
            </div>
        );
    }
}

// 設定 prop 初始值 方法2
Counter.defaultProps = {
    initCount: 0
}
// 設定 prop 值的類型 方法2
Counter.propTypes = {
    initCount: PropTypes.number
}

export default Counter;
