import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import Message from "./Message";
import List from "./List";
import Count from "./Count";
import Counter from "./Counter";
import Counter_callback from "./Counter_callback";
import Input from "./Input";
import Parent from "./parent_5_4";
import Toggle from "./Toggle";

// react 18 version
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
    <React.StrictMode>
        <Toggle/>
        {/*<Counter_callback initCount={0}/>*/}
        {/*<List/>*/}
    </React.StrictMode>
);
