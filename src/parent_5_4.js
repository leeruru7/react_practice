import React, {Component} from 'react';
import Child from './child_5_4'

class Parent extends Component {
    state = {
        count: 0,
        childCount: 0
    }
    addCount = () => {
        this.setState({
            count: this.state.count + 1
        })
    }
    addChildCount = () => {
        this.setState({
            childCount: this.state.childCount + 1
        })
    }

    render() {
        return (
            <div>
                <h1>Parent: {this.state.count}</h1>
                <button onClick={this.addCount}>+parent</button>
                <button onClick={this.addChildCount}>+child</button>
                {/*props方式 addParentCount 讓子組件執父組件的this.addCount*/}
                <Child
                    count={this.state.childCount}
                    addParentCount={this.addCount}
                    addChildCount={this.addChildCount}
                />
            </div>
        );
    }
}

export default Parent;
