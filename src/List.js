import React, {Component} from 'react';
import Item from "./Item";

class List extends Component {
    render() {
        return (
            <ol>
                <Item text="Learn React"/>
                <Item text="Learn TypeScript"/>
                <Item text="Make money"/>
            </ol>
         )
    }
}

export default List;
