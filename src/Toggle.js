import React, {Component} from 'react';
import paoz from './image.png'
import './style.css'

class Toggle extends Component {
    state = {
        visible: true
    }
    toggle = () => {
        this.setState({
            visible: !this.state.visible
        })
    }

    render() {
        const {visible} = this.state;
        const styleObj = {display: visible ? 'block' : 'none'};
        const cn = `image ${visible ? '' : 'hide'}`;
        return (
            <div>
                <button onClick={this.toggle}>Toggle</button>
                <hr/>
                {/*三元判斷式 1*/}
                {/*{visible ? <img src={paoz} alt=""/> : null}*/}

                {/*三元判斷式 2*/}
                {/*{visible && <img src={paoz} alt=""/>}*/}

                {/*style 方法*/}
                {/*<img style={styleObj}  src={paoz} alt=""/>*/}

                {/*className 方法*/}
                <img className={cn} src={paoz} alt=""/>
            </div>

        );
    }
}

export default Toggle;
