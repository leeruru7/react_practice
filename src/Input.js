import React, {Component,createRef} from 'react';

class Input extends Component {

    // react 17 老師的教法
    // setRef = (input) =>{
    //     input.focus();
    // }

    // 官方推薦
    myInput = createRef();
    componentDidMount() {
        this.myInput.current.focus();
    }

    render() {
        return (
            <div>
                <h3>enter your name</h3>
                {/*老師的教法 <input type="text" ref={this.setRef}/>*/}
                {/*官方推薦*/}
                <input type="text" ref={this.myInput}/>
            </div>
        );
    }
}

export default Input;
