import React, {Component} from 'react';

class Message extends Component {
    state = {
        text: 'Hello'
    }

    sayHi = () => {
        this.setState({
            text: 'Hi'
        })
    }

    render() {
        return (
            <div>
                <h2>{this.state.text}</h2>
                <button onClick={this.sayHi}>click me</button>
            </div>
        );
    }
}

export default Message;
