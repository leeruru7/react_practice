import React, {Component} from 'react';

class Count extends Component {
    state = {
        count: 0,
        index: 1
    }
    addCount = () => {
        const {count, index} = this.state;
        this.setState({
            index: index + 1,
            count: count + index
        })
    }

    render() {
        const {count, index} = this.state;
        return (
            <div>
                <h1>計數器練習</h1>
                <h3>count:{count}</h3>
                <button onClick={this.addCount}>+{index}</button>
            </div>
        )
    }
}

export default Count;
