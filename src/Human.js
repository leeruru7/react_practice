// class 寫法

import React, {Component} from 'react';

// 定義一個 item 類別
// 類別裡可建立多個實例
// static property
// static method
class Human extends Component {
    // 定義一般屬性
    proTypes = {}
    // 定義靜態屬性
    static proTypes = {}

    // 定義靜態方法
    static getDataFromProps(nextProps, prevState) {

    }
}

// 在類別外也可以定義屬性
Human.propType = {

}
