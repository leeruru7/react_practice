import React, {Component} from 'react';
import PropTypes from 'prop-types';

class Counter_callback extends Component {
    // 用 props 指定初始 state
    constructor(props) {
        super(props); // 就是 counter 這個 component
        this.state = {
            count: props.initCount,
        }
    }

    addCount = () => {
        // setState傳入callback function
        this.setState({
            count: this.state.count + 1,
        }, () => {
            // count值改變才會執行這邊 callback function
            this.sendCount();
        })

        // 這種寫法會造成，fetch時拿到的count是舊的資料，因為setState是非同步，他不會執行完才操作this.sendCount()
        // this.setState({
        //     count: this.state.count + 1,
        // })
        //
        // this.sendCount();
    }

    sendCount = () => {
        fetch(`/api/count?value=${this.state.count}`);
    }

    render() {
        return (
            <div>
                <h1>{this.state.count}</h1>
                <button onClick={this.addCount}>+1</button>
            </div>
        );
    }
}

// 設定 prop 初始值 方法2
Counter_callback.defaultProps = {
    initCount: 0
}
// 設定 prop 值的類型 方法2
Counter_callback.propTypes = {
    initCount: PropTypes.number
}

export default Counter_callback;
