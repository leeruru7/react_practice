const circleArea = radius => radius * radius * Math.PI;

const visitMessage = visitor => {
    const age = visitor.age;
    const name = visitor.name;
    if (age < 18) {
        return `Dear ${name}, you are under age`
    }
    return `Welcome, ${name}!`
}

const getVector = (point, axis) => {
    return { [axis]: point[axis] };
};
const p = { x: 1, y: 2, z: 3 };
const q = getVector(p, 'x');
console.log(q)
